const mostrarTodos=(data)=>{
    console.log(data)

    const res = document.getElementById('respuesta');
    res.innerHTML="";

    for(let item of data){
        res.innerHTML+=item.userId+","+item.id+","+item.title+","+item.completed+"<br>";
    }
}
document.getElementById('btnLimpiar').addEventListener('click',function(){
    limpiarDatos();
})
document.getElementById('btnCargarP').addEventListener('click',function(){
    llamadoFetch();
}
)
document.getElementById('btnCargarA').addEventListener('click',function(){
    llamandoAwait();
}
)
document.getElementById('btnCargarAx').addEventListener('click',function(){
    llamandoAxion();
}
)
const llamadoFetch = () => {
    const url = "https://jsonplaceholder.typicode.com/todos";
    fetch(url)
        .then(respuesta => respuesta.json())
        .then(data => mostrarTodos(data))
        .catch(reject => {
            const lblError = document.getElementById('lblError');
            lblError.innerHTML = "Surgió un error" + reject;
        });
}

//await
const llamandoAwait = async ()=>{
    try{
        const url = "https://jsonplaceholder.typicode.com/todos";
        const respuesta = await fetch(url)
        const data = await respuesta.json()
        mostrarTodos(data);
    }
    catch(error){
        console.log("Surgio un error");
    }
}

//Axion
const llamandoAxion = async()=>{
const url = "https://jsonplaceholder.typicode.com/todos";
axios
.get(url)
.then((res)=>{
    mostrarTodos(res.data)
})
.catch((err)=>{
    console.log("Surgio un error"+ err);
})
}

const limpiarDatos=async()=>{
    res=document.getElementById('respuesta');
    res.innerHTML= " ";
}